# README #

This is the supplementary Python code for our paper submitted to Frontiers in Microbiology.
### What is this repository for? ###

This code includes stepwise regression of a gLV model by miniminzing the BIC.

### How do I get set up? ###

Required python packages:
numpy,scipy, collections, lmfit,pandas

### How to run? ###

python Code_1.py ${fdata} ${percent} ${fout}

### fdata: input dataset file (e.g., Supplementary_data_1.txt); percent: the proportion of training dataset； fout: output file.